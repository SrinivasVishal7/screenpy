import struct

import cv2
import numpy
from mss import mss
import socket
import pickle


class ScreenGrabber:
    def __init__(self, resolution, host, port):
        self.clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket.connect((host, port))
        self.mon = {'top': 0, 'left': 0, 'width': resolution[0], 'height': resolution[1]}
        self.sct = mss()

    def grabber(self):
        while 1:
            screen = numpy.array(self.sct.grab(self.mon))
            screen_data = pickle.dumps(screen)
            self.clientsocket.sendall(struct.pack("L", len(screen_data)) + screen_data)
            self.clientsocket.send(screen)
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
